The files in this repository are meant to be used to compile the Reality plugin project. 

These are the special versions of Qt that I used in the compilation of Reality. To use 
them, simply download the zipfiles, explode then and copy the resulting directory in a 
location of your choice. You will then need to adjust the build scripts in the Reality
repository to point to these librarries.

--
Paolo Ciccone
Oct 8th, 2019
